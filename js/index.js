$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 2000
    });

    $('#contacto').on('show.bs.modal', function (e) {
    
      console.log('El modal contacto está mostrando');
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-black');
      $('#contactoBtn').prop('disabled',  true);
    });
    $('#contacto').on('shown.bs.modal', function(e){
      console.log('El modal  se mostro está mostrando');
    });
    $('#contacto').on('hide.bs.modal', function (e) {
    
      console.log('El modal contacto se oculta');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
      console.log('El modal  se mostro se ocultó');
      $('#contactoBtn').addClass('btn-outline-success');
      $('#contactoBtn').prop('disabled',  false);
    });


  });